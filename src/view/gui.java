package view;

import java.awt.BorderLayout;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTextArea;

public class gui extends JFrame {

	private JTextArea showResults;
	private JButton endButton;
	private String str;

	public gui() {
		createFrame();
	}

	public void createFrame() {
		this.showResults = new JTextArea("your resutls will be showed here");
		this.endButton = new JButton("end program");
		setLayout(new BorderLayout());
		add(this.showResults);
		add(this.endButton, BorderLayout.SOUTH);
	}

	public void setResult(String str) {
		this.str = str;
		showResults.setText(this.str);
	}

	public void extendResult(String str) {
		this.str = this.str + "\n" + str;
		showResults.setText(this.str);
	}

	public void setListener(ActionListener list) {
		endButton.addActionListener(list);
	}

}
