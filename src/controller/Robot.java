package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import model.translateAscii;
import view.gui;



public class Robot {
	
	
	class ListenerMgr implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			System.exit(0);

		}

	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new Robot();
		
	}

	public Robot() {
		frame = new gui();
		frame.pack();
		frame.setVisible(true);
		frame.setSize(900, 900);
		list = new ListenerMgr();
		frame.setListener(list);
		setTestCase();
	}

	public void setTestCase() {
		
		String url1 = "https://docs.google.com/document/d/1D34SimpuyAX_PXjnhPv5pRlVK8xLvqPnYdTkdOSN-h8/edit1";
		translateAscii a = new translateAscii();
		String url2 = "https://docs.google.com/document/d/1D34SimpuyAX_PXjnhPv5pRlVK8xLvqPnYdTkdOSN-h8/edit2";
		translateAscii b = new translateAscii();
		String url3 = "https://docs.google.com/document/d/1D34SimpuyAX_PXjnhPv5pRlVK8xLvqPnYdTkdOSN-h8/edit3";
		translateAscii c = new translateAscii();
		String url4 = "https://docs.google.com/document/d/1D34SimpuyAX_PXjnhPv5pRlVK8xLvqPnYdTkdOSN-h8/edit4";
		translateAscii d = new translateAscii();
		String url5 = "https://docs.google.com/document/d/1D34SimpuyAX_PXjnhPv5pRlVK8xLvqPnYdTkdOSN-h8/edit5";
		translateAscii e = new translateAscii();
		String url6 = "https://docs.google.com/document/d/1D34SimpuyAX_PXjnhPv5pRlVK8xLvqPnYdTkdOSN-h8/edit6";
		translateAscii f = new translateAscii();
		String url7 = "https://docs.google.com/document/d/1D34SimpuyAX_PXjnhPv5pRlVK8xLvqPnYdTkdOSN-h8/edit7";
		translateAscii g = new translateAscii();
		String url8 = "https://docs.google.com/document/d/1D34SimpuyAX_PXjnhPv5pRlVK8xLvqPnYdTkdOSN-h8/edit8";
		translateAscii h = new translateAscii();
		String url9 = "https://docs.google.com/document/d/1D34SimpuyAX_PXjnhPv5pRlVK8xLvqPnYdTkdOSN-h8/edit9";
		translateAscii m = new translateAscii();
		String url10 = "https://docs.google.com/document/d/1D34SimpuyAX_PXjnhPv5pRlVK8xLvqPnYdTkdOSN-h8/edit10";
		translateAscii j = new translateAscii();
		String url11 = "https://docs.google.com/document/d/1D34SimpuyAX_PXjnhPv5pRlVK8xLvqPnYdTkdOSN-h8/edit11";
		translateAscii k = new translateAscii();
		String url12 = "https://docs.google.com/document/d/1D34SimpuyAX_PXjnhPv5pRlVK8xLvqPnYdTkdOSN-h8/edit12";
		translateAscii l = new translateAscii();
		
		
		frame.setResult("URL is :                     "+url1);
		frame.extendResult("Sum of Ascii is :      "+a.sumAscii(url1));
		frame.extendResult("Hash is :                   "+a.modSumAscii());
		frame.extendResult("URL is :                     "+url2);
		frame.extendResult("Sum of Ascii is :      "+b.sumAscii(url2));
		frame.extendResult("Hash is :                   "+b.modSumAscii());
		frame.extendResult("URL is :                     "+url3);
		frame.extendResult("Sum of Ascii is :      "+c.sumAscii(url3));
		frame.extendResult("Hash is :                   "+c.modSumAscii());
		frame.extendResult("URL is :                     "+url4);
		frame.extendResult("Sum of Ascii is :      "+d.sumAscii(url4));
		frame.extendResult("Hash is :                   "+d.modSumAscii());
		frame.extendResult("URL is :                     "+url5);
		frame.extendResult("Sum of Ascii is :      "+e.sumAscii(url5));
		frame.extendResult("Hash is :                   "+e.modSumAscii());
		frame.extendResult("URL is :                     "+url6);
		frame.extendResult("Sum of Ascii is :      "+f.sumAscii(url6));
		frame.extendResult("Hash is :                   "+f.modSumAscii());
		frame.extendResult("URL is :                     "+url7);
		frame.extendResult("Sum of Ascii is :      "+g.sumAscii(url7));
		frame.extendResult("Hash is :                   "+g.modSumAscii());
		frame.extendResult("URL is :                     "+url8);
		frame.extendResult("Sum of Ascii is :      "+h.sumAscii(url8));
		frame.extendResult("Hash is :                   "+h.modSumAscii());
		frame.extendResult("URL is :                     "+url9);
		frame.extendResult("Sum of Ascii is :      "+m.sumAscii(url9));
		frame.extendResult("Hash is :                   "+m.modSumAscii());
		frame.extendResult("URL is :                     "+url10);
		frame.extendResult("Sum of Ascii is :      "+j.sumAscii(url10));
		frame.extendResult("Hash is :                   "+j.modSumAscii());
		frame.extendResult("URL is :                     "+url11);
		frame.extendResult("Sum of Ascii is :      "+k.sumAscii(url11));
		frame.extendResult("Hash is :                   "+k.modSumAscii());
		frame.extendResult("URL is :                     "+url12);
		frame.extendResult("Sum of Ascii is :      "+l.sumAscii(url12));
		frame.extendResult("Hash is :                   "+l.modSumAscii());
		
		
		


	}

	ActionListener list;
	gui frame;
}


		


